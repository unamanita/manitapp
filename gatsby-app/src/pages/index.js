import React, { useState } from 'react';
import { graphql } from 'gatsby';
import _ from 'lodash';
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import useGeolocation from '@rooks/use-geolocation';
import Layout from '../components/ui/Layout';
import CentralButton from '../components/ui/CentralButton';
import BaseMap from '../components/maps/BaseMap';
/**
 * Extracts menu from result of graphql done on page
 * query from index, which is how i like it.
 *
 * @param {*} elements
 */
const extMenu = (elements) => (_(elements)
  .sortBy('meta.order')
  .map(({ label, description, meta: { internalName } }) => (
    {
      internalName,
      label,
      description,
    }
  ))
  .value());

function index(data) {
  const {
    data:
    {
      mainMenu:
        { elements },
    },
  } = data;
  const whereAreWe = useGeolocation();
  const clickWannaHelp = () => {

  };
  const clickNeedHelp = () => {

  };
  return (
    <Layout gatsbyPageData={data} navMenu={extMenu(elements)}>
      <Container>
        <Row className='p-1'>
          <Col>
            <CentralButton
              internalName="wannahelp"
              onClick={() => clickWannaHelp()}
            />
          </Col>
          <Col>
            <CentralButton
              internalName="needhelp"
              onClick={() => clickNeedHelp()}
            />
          </Col>
        </Row>
        <Row>
          <Col>
            <BaseMap />
          </Col>
        </Row>
      </Container>
    </Layout>
  );
}


export const query = graphql`
  {
    mainMenu: allDirectusMainElementTranslation(filter: {language: {eq: "es"}, main_element: {area: {eq: "navbar"}}}) {
      elements: nodes {
        label
        language
        description
        meta: main_element {
          internalName:internal_name
          order
          area
        }
      }
    }
  }
`;

export default index;
