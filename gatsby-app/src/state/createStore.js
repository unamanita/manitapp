import { createStore } from 'redux';
import reducer from './reducer';


// preloadedState will be passed in by the plugin
export default (preloadedState) => {
  if (typeof window !== 'undefined') {
    return createStore(reducer,
      preloadedState,
      window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
  }
  return createStore(reducer,
    preloadedState);
};
