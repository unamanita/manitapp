
// Create initial state by graphql because that is how we roll baby
const state = {};


function reducer(initialState = state, action) {
  // ...
  switch (action.type) {
    default:
      return (initialState);
  }
}

export default reducer;
