import React from 'react';
import PropTypes from 'prop-types';
import Button from 'react-bootstrap/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faHandHoldingHeart,
  faHandHolding,
  faHome,
} from '@fortawesome/free-solid-svg-icons';

/**
 * Hardcodes the correct fontawesome component
 * for a given entry of the main_elements table
 *
 * @param {*} mainElemInternalName
 */
const iconFor = (mainElemInternalName) => {
  switch (mainElemInternalName) {
    case 'home':
      return (<FontAwesomeIcon icon={faHome} />);

    case 'wannahelp':
      return (<FontAwesomeIcon icon={faHandHoldingHeart} />);

    case 'needhelp':
      return (<FontAwesomeIcon icon={faHandHolding} />);

    default:
      return null;
  }
};

function CentralButton(props) {
  const {
    internalName, label, description,onClick
  } = props;
  return (
    <Button as="span" block>
      {iconFor(internalName)}
    </Button>
  );
}

CentralButton.propTypes = {
  internalName: PropTypes.string,
  label: PropTypes.string,
  description: PropTypes.string,
  onClick:PropTypes.func
};

export default CentralButton;
