
import React from 'react';
import PropTypes from 'prop-types';
import Container from 'react-bootstrap/Container';
import { Row } from 'react-bootstrap';
import NavBar from './Navbar';


function Layout(props) {
  const {
    gatsbyPageData,
    navMenu,
    children,
  } = props;
  return (
    <Container>
      <Row>
        <NavBar navMenu={navMenu} />
      </Row>
      <Row>
        {children}
      </Row>
    </Container>
  );
}

Layout.propTypes = {
  navMenu: PropTypes.arrayOf(PropTypes.string),
  children: PropTypes.arrayOf(PropTypes.element),
  gatsbyPageData: PropTypes.object,
};

export default Layout;
