import '../../styles/style.scss';
import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import PropTypes from 'prop-types';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Button from 'react-bootstrap/Button';
import { useStaticQuery, graphql, Link } from 'gatsby';
import _ from 'lodash';


const NavLink = (internalName, label) => (
  <Nav.Link href={`#${internalName}`}>
    {`${label}`}
  </Nav.Link>
);

function NavBar(props) {
  const { navMenu } = props;
  console.log('navMenu');
  console.log(navMenu);
  return (
    <Navbar bg="dark" variant="dark" expand="md">
      <Navbar.Brand href="#home">Una manita!</Navbar.Brand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav className="mr-auto">
          {navMenu.map(({ internalName, label }) => NavLink(internalName, label))}
          <Form inline>
            <FormControl type="text" placeholder="Search" className="mr-sm-2" />
            <Button variant="outline-info">Search</Button>
          </Form>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}

Navbar.propTypes = {
  navMenu: PropTypes.arrayOf(PropTypes.string),
};

export default NavBar;
