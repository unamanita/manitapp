import React from 'react';
import { useLeafletMap, useLeafletZoom, useLeafletIsZooming } from 'use-leaflet';
import useGeolocation from '@rooks/use-geolocation';
import {
  Map, TileLayer, Marker, Tooltip,
} from 'react-leaflet';

const ZoomTooltip = (props) => {
  const zoom = useLeafletZoom();
  const isZooming = useLeafletIsZooming();
  return (
    <Tooltip {...props}>
      {isZooming ? '.....zooming.....' : `Zoom = ${zoom}`}
    </Tooltip>
  );
};

/**
 *
 *
 * @param {*} props
 * @returns
 */
const BaseMap = (props) => {
  const geo = useGeolocation();
  console.log('geo');
  console.log(geo);
  return (
    <div>
      <Map center={[geo.lat, geo.lng]} zoom={13}>
        <TileLayer
          attribution='<a href="https://github.com/vadzim/use-leaflet" title="React hooks for react-leaflet">use-leaflet</a> | &amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.osm.org/{z}/{x}/{y}.png"
        />
        <Marker position={[51.505, -0.091]} />
        <Marker position={[51.515, -0.081]}>
          <ZoomTooltip permanent />
        </Marker>
      </Map>
    </div>
  );
};

export default BaseMap;
